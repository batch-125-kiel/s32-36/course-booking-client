//target the form
	//getElementById
	//getElementByClass
	//getElementByTag

let registerForm = document.querySelector('#registerUser');

//add event listener
registerForm.addEventListener("submit", (e)=>{

	e.preventDefault();

//target the elements of the form and get their values
let firstName =document.getElementById('firstName').value;
let lastName =document.getElementById('lastName').value;
let mobileNo =document.getElementById('mobileNo').value;
let email =document.getElementById('email').value;
let password =document.getElementById('password').value;
let password2 =document.getElementById('password2').value;

	if(password === password2 && mobileNo.length === 11 && password !=="" && password2 !==""){

		//fetch(url, {options}).then()

		fetch("http://localhost:3000/api/users/checkEmail",{

			method:"POST",
			headers:{
				"Content-Type": "application/json"
			},
			body:JSON.stringify({
				//from front end
				email: email
			})

		})
		.then(result=>result.json())
		.then(result =>{
			//result === false
				//means this can be saved in the database

			if(result === false){

				fetch("http://localhost:3000/api/users/register",{
					method:"POST",
					headers:{
						"Content-Type": "application/json"
					},

					body:JSON.stringify({


						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password


					})


				})
				.then(result=> result.json())
				.then(result =>{

					if(result === true){

						alert('Succesfully added');

						//redirect to login
						window.location.replace('./../pages/login.html')
					}else{

						alert('Error adding');

					}


				})

			}else{

				alert('Email already exists');
			}

		})


	}

});

