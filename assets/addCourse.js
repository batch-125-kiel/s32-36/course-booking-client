let createCourse = document.querySelector('#createCourse');

createCourse.addEventListener("submit",(e)=>{

	e.preventDefault();

	let courseName = document.querySelector('#courseName').value;
	let coursePrice = document.querySelector('#coursePrice').value;
	let courseDesc = document.querySelector('#courseDesc').value;

	let token = localStorage.getItem("token");

	fetch('http://localhost:3000/api/courses/add',{

		method: "POST",
		headers: {

			"Content-Type": "application/json",
			//authorization to verify if logged in?
			"Authorization": `Bearer ${token}`
		},
		body: JSON.stringify({

			name: courseName,
			price: coursePrice,
			description: courseDesc
		})
	})

	.then(result => result.json())
	.then(result => {

		if(result){

			alert('Course succesfully added');
			window.location.replace('./courses.html');
		}else{

			alert('Course creation failed. Something went wrong');
		}

	})

})
